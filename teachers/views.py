from django.views.generic import UpdateView, ListView, CreateView, DeleteView
from django.urls import reverse_lazy

from core.views import LoginRequiredBaseMixin
from .models import Teacher
from .forms import TeacherCreateForm


# Create your views here.
class TeacherList(ListView):
    model = Teacher
    context_object_name = 'teachers'
    template_name = 'teachers/list.html'
    pk_url_kwarg = 'uuid'
    paginate_by = 5

    def get_queryset(self):
        qs = super().get_queryset()
        request = self.request
        params = [
            'first_name',
            'last_name',
        ]
        for param in params:
            value = request.GET.get(param)
            if value:
                qs = qs.filter(**{param: value})
        return qs


class TeacherEditView(LoginRequiredBaseMixin, UpdateView):
    login_url = 'accounts:login'
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers/edit.html'
    success_url = reverse_lazy('teachers:list')
    context_object_name = 'teacher'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class TeacherCreateView(LoginRequiredBaseMixin, CreateView):
    login_url = 'accounts:login'
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'tachers/create.html'
    success_url = reverse_lazy('tachers:list')
    context_object_name = 'teacher'
    pk_url_kwarg = 'uuid'


class TeacherDeleteView(LoginRequiredBaseMixin, DeleteView):
    login_url = 'accounts:login'
    model = Teacher
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)
