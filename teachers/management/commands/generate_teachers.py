from django.core.management.base import BaseCommand
from teachers.models import Teacher
from faker import Faker


class Command(BaseCommand):
    help = "Generates teachers and add them to database(using faker)"

    def add_arguments(self, parser):
        parser.add_argument(
            'number',
            type=int,
            help="Number of Teachers to generate"
            )

    def handle(self, *args, **options):
        fake = Faker()
        for _ in range(options['number']):
            Teacher.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                birthdate=fake.date_of_birth(tzinfo=None, minimum_age=20, maximum_age=99),
                )
