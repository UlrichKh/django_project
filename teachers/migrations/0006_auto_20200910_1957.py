# Generated by Django 3.1 on 2020-09-10 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0005_auto_20200910_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='uuid',
            field=models.UUIDField(default='4a20d1931cc94deead49917f02d3f0d1', unique=True),
        ),
    ]
