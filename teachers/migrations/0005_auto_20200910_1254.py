# Generated by Django 3.1 on 2020-09-10 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0004_auto_20200910_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='uuid',
            field=models.UUIDField(default='38b7a1262bf1499298ccecc645e2c126', unique=True),
        ),
    ]
