from django.contrib import admin
from .models import Teacher
from groups.models import Group


# Register your models here.
class GroupTable(admin.TabularInline):
    model = Group
    fields = ['course', 'starting_date']
    read_only = fields
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    exclude = ('uuid',)
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
