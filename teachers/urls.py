from django.urls import path

from . import views

app_name = "teachers"

urlpatterns = [
    path('list', views.TeacherList.as_view(), name='list'),
    path('create', views.TeacherCreateView.as_view(), name="create"),
    path('edit/<str:uuid>', views.TeacherEditView.as_view(), name="edit"),
    path('delete/<str:uuid>', views.TeacherDeleteView.as_view(), name="delete")
]
