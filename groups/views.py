
from django.shortcuts import reverse
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.views.generic import UpdateView, ListView, CreateView, DeleteView
from django.urls import reverse_lazy

from core.views import LoginRequiredBaseMixin
from .models import Group
from .forms import GroupCreateForm
from core.utils import parse_count


# Create your views here.
class GroupList(ListView):
    model = Group
    context_object_name = 'groups'
    template_name = 'groups/list.html'
    RELATED = ['headman', 'curator']
    paginate_by = 5

    def get_object(self):
        obj = super().get_object()
        for param in self.RELATED:
            obj.select_related(param)
        return obj


class GroupEditView(LoginRequiredBaseMixin, UpdateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups/edit.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class GroupCreateView(LoginRequiredBaseMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups/create.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'
    pk_url_kwarg = 'uuid'


class GroupDeleteView(LoginRequiredBaseMixin, DeleteView):
    model = Group
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


def generate_groups(request):
    try:
        count = parse_count(request)
    except Exception as ex:
        return HttpResponseBadRequest(str(ex))
    for _ in range(count):
        Group.generate_group()
    return HttpResponseRedirect(reverse('groups:list'))
