import random
import datetime as dt

from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError
from django.db import models

from core.models import BaseModel
from students.models import Student
from teachers.models import Teacher
from core.utils import generate_uuid


# Create your models here.
class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )

    def __str__(self):
        return f"{self.name}, {self.floor}"


class Group(BaseModel):
    MAX_STUDENTS = 16
    group_number = models.IntegerField()
    course = models.CharField(max_length=128, default="unnamed course")
    curator = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)
    students = models.ManyToManyField(Student, related_name='groups')
    headman = models.OneToOneField(Student, on_delete=models.SET_NULL, null=True)
    classroom = models.ManyToManyField(Classroom, related_name='cls_groups')
    starting_date = models.DateTimeField()
    uuid = models.UUIDField(default=generate_uuid, unique=True)

    def clean(self):
        if self.id:
            if self.headman not in self.students.all():
                raise ValidationError('Head of the Group not Group\'s student')

    def __str__(self):
        return f"{self.course}, {self.starting_date.date()}"

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    @staticmethod
    def generate_group(students_count=MAX_STUDENTS):
        COURSE_NAME = [
            "Cyber Security Course",
            "Python Basic Course",
            "Python Advanced Course",
            "Learn All Programming Languages in 10 seconds",
            "'Hello, World!' Crash Course",
        ]
        group_number = random.randint(1000, 1999)
        course = random.choice(COURSE_NAME)
        curator = random.choice(Teacher.objects.all())
        students = []
        students_query = Student.objects.all()
        for _ in range(students_count):
            students.append(random.choice(students_query))
        starting_date = dt.date.today()
        headman = Student.objects.filter(groups__isnull=True).order_by('?').first()

        gr = Group(
            group_number=group_number,
            course=course,
            curator=curator,
            headman=headman,
            starting_date=starting_date,
        )
        gr.save()
        gr.students.add(*students)
        gr.save()
