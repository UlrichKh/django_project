from django.contrib import admin  # noqa

from .models import Classroom, Group
# from students.models import Student


# Register your models here.
# class StudentTable(admin.TabularInline):
#     model = Student
#     fields = ['first_name', 'last_name']
#     read_only = fields
#     show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['group_number', 'course', 'headman']
    fields = ['group_number', 'course', 'curator', 'headman', 'classroom', 'students']
    # inlines = [StudentTable]
    field_select_related = ['headman']


admin.site.register(Classroom)
admin.site.register(Group, GroupAdmin)
