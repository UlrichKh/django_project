from django.urls import path

from . import views

app_name = "groups"

urlpatterns = [
    path('list', views.GroupList.as_view(), name='list'),
    path('create', views.GroupCreateView.as_view(), name='create'),
    path('edit/<str:uuid>', views.GroupEditView.as_view(), name='edit'),
    path('delete/<str:uuid>', views.GroupDeleteView.as_view(), name='delete'),
    path('generate_groups', views.generate_groups, name='generate'),
]
