from django.core.management.base import BaseCommand
from groups.models import Group


class Command(BaseCommand):
    help = "Generates Groups with random students and teachers from db"

    def add_arguments(self, parser):
        parser.add_argument(
            'number',
            type=int,
            help="Number of Groups to generate"
            )

    def handle(self, *args, **options):
        for _ in range(options['number']):
            Group.generate_group()
