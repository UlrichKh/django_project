from math import sqrt
from random import randint


# Home work about Shapes
class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    @staticmethod
    def compute_dist(x1, x2, y1, y2):
        x = abs(x2-x1)
        y = abs(y2-y1)
        result = sqrt(x**2+y**2)
        return result

    def __contains__(self, point):
        if not isinstance(point, Point):
            raise ValueError('point is not Point class')
        checked_point = self.compute_dist(point.x, self.x, point.y, self.y)
        if self.radius <= checked_point:
            return True
        else:
            return False


c1 = Circle(randint(1, 10), randint(1, 10), randint(1, 20))
point = Point(randint(1, 20), randint(1, 20))
if point in c1:
    print('Point in Circle')
else:
    print('Point out of Circle')


# Home work about Robots
class Robot:
    def __init__(self):
        self.battery_capacity = 200000
        self.gyroscope = True
        self.material = "aluminium"

    def system_check(self):
        print(f"""
        {type(self).__name__} - Online

        power and battery - Ok
        sensors - Ok
        network connection - Ok
        software - Ok

        System check complete - System working
        """)


class SpotMini(Robot):
    def __init__(self, model):
        super().__init__()
        self.model = model
        self.legs = 4
        self.info = []

    def scout(self, info):
        self.info.append(info)
        print(f"Found {info}")


class Atlas(Robot):
    def __init__(self, model):
        super().__init__()
        self.model = model
        self.legs = 2
        self.hands = 2

    def act_like_human(self):
        print(f"""
        Hi there! I am Human!
        Sure! I have {self.legs} legs and {self.hands} hands!
        """)


class Handle(Robot):
    def __init__(self, model):
        super().__init__()
        self.model = model
        self.swl = 10  # safe working load = 10 kg

    def move_boxes(self, place1, place2):
        print(f'Stating to move boxes from {place1} to {place2}.')


sm = SpotMini('Dog-2203')
sm.system_check()
sm.scout("banana")
sm.scout("door")
sm.scout("ball")

at = Atlas('T1000')
at.system_check()
at.act_like_human()


hl = Handle('Crane-001')
hl.system_check()
hl.move_boxes('storage', 'carpentry')


# Q task
class Q:
    def __init__(self, **params):
        self._params = []
        self._params.append(params)

    @classmethod
    def to_string(cls, d):
        result = ''
        for item in d:
            if isinstance(item, list):
                if len(item) == 1 or item[0] == ' NOT ':
                    result += cls.to_string(item)
                else:
                    result += f'({cls.to_string(item)})'
            elif isinstance(item, dict):
                for k, v in item.items():
                    result += f"{k}={repr(v)}"
            else:
                result += item
        return result

    def __or__(self, other):
        self._params.append(' OR ')
        self._params.append(other._params)
        return self

    def __and__(self, other):
        self._params.append(' AND ')
        self._params.append(other._params)
        return self

    def __ne__(self, other):
        if len(self._params) > 1:
            nt = ' AND NOT '
        else:
            nt = ' NOT '
        self._params.append(nt)
        self._params.append(other._params)
        return self

    def __invert__(self):
        params = self._params
        self._params = [' NOT ', params]
        return self

    def __str__(self):
        return self.to_string(self._params)


filter = Q()
filter = Q(first_name='Jhon') | (Q(last_name='c') & ~Q(email='jsmith@gmail.com'))
print(filter)
filter = (Q(first_name='Jhon') | Q(last_name='Smith')) & ~Q(email='jsmith@gmail.com')
print(filter)
filter = ~(Q(first_name='Jhon') | Q(last_name='Smith')) & Q(email='jsmith@gmail.com')
print(filter)
