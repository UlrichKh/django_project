from django.contrib import admin # noqa

from .models import UserAction, Profile
# Register your models here.admin.site.register(User)
admin.site.register(UserAction)
admin.site.register(Profile)
