import os
import sys

from io import BytesIO
from PIL import Image

from django.contrib.auth.models import User
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models  # noqa

from lms.settings import BASE_DIR


# Create your models here.


class UserAction(models.Model):
    class USER_ACTION(models.IntegerChoices):
        LOGIN = 0, "Login"
        LOGOUT = 1, "Logout"
        CHANGE_PASSWORD = 2, "Change Password"
        CHANGE_PROFILE = 3, "Change Profile"
        CHANGE_PROFILE_IMAGE = 4, "Change Profile Image"

    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    write_date = models.DateTimeField(auto_now_add=True)
    action = models.PositiveSmallIntegerField(choices=USER_ACTION.choices)
    info = models.CharField(max_length=128, null=True)

    @classmethod
    def record_action(cls, request, action, info=None):
        user = request.user
        if info is None:
            info = f"{ user.username } performed an action: { action }"
        usac = cls(
            user=user,
            action=action,
            info=info
        )
        usac.save()

    @classmethod
    def record_image_action(cls, request, old, new):
        info = f'{request.user.username} changed picture from "{old}" to "{new}"'
        cls.record_action(
            request=request,
            action=UserAction.USER_ACTION.CHANGE_PROFILE_IMAGE,
            info=info
        )

    def __str__(self):
        return f'{self.info}'


class Profile(models.Model):
    PROFILE_IMAGES_DIRECTORY = os.path.join(BASE_DIR, 'media/profile_pics/')
    MAX_SIZE_PX = 300
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        related_name='profile'
    )
    image = models.ImageField(null=True, default='default.jpeg', upload_to='profile_pics/')
    interests = models.CharField(max_length=128, null=True)

    def save(self, *args, **kwargs):
        self.image = self.resize_profile_image(self.image, self.MAX_SIZE_PX)
        saver = super().save(*args, **kwargs)
        return saver

    def __str__(self):
        return f'{self.user.username}\'s profile'

    @staticmethod
    def resize_profile_image(image, max_size):
        with Image.open(image) as img:
            output = BytesIO()
            if (img.size[0] or img.size[1]) > max_size:
                img.thumbnail((max_size, max_size))
                img.save(output, format='jpeg')
                return InMemoryUploadedFile(
                    (pic := output), # noqa
                    'ImageField',
                    image.name,
                    'image/jpeg',
                    sys.getsizeof(pic),
                    None
                )
            return image
