from accounts.models import Profile
from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms.models import ModelForm


class AccountCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ]


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
        ]


class AccountPasswordForm(PasswordChangeForm):
    pass


class AccountProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'interests']
