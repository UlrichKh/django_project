from django.shortcuts import HttpResponseRedirect, render # noqa
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib import messages

from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView
from django.urls import reverse_lazy, reverse

from .forms import AccountCreateForm, AccountPasswordForm, AccountProfileForm, AccountUpdateForm
from .models import Profile, UserAction

# Create your views here.


class AccountCreateView(CreateView):
    model = User
    template_name = 'accounts/registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('index')


class AccountLoginView(LoginView):
    template_name = 'accounts/login.html'

    def get_redirect_url(self):
        return reverse('index')

    def form_valid(self, form):
        valid = super().form_valid(form)
        UserAction.record_action(request=self.request, action=UserAction.USER_ACTION.LOGIN)
        if valid:
            messages.success(self.request, 'Logged in Successfully.')
            try:
                profile = self.request.user.profile
            except Exception:
                profile = Profile.objects.create(user=self.request.user)
                profile.save()
        return valid


class AccountLogoutView(LogoutView):
    template_name = 'accounts/logout.html'

    # def get_redirect_url(self):
    #     if self.request.GET.get('next'):
    #         return self.request.GET
    #     return reverse('index')

    def form_valid(self, form):
        UserAction.record_action(request=self.request, action=UserAction.USER_ACTION.LOGOUT)
        if (valid := super().form_valid(form)): # noqa
            messages.warning(self.request, 'Account Logged out.')
        return valid


# class AccountUpdateView(UpdateView):
#     model = User
#     template_name = 'accounts/profile.html'
#     form_class = AccountUpdateForm
#     success_url = reverse_lazy('index')

#     def get_object(self, queryset=None):
#         return self.request.user

#     def form_valid(self, form):
#         UserAction.record_action(request=self.request, action=UserAction.USER_ACTION.CHANGE_PROFILE)
#         if (valid := super().form_valid(form)): # noqa
#             messages.success(self.request, 'Account Changed Successfully.')
#         return valid


class AccountPasswordView(PasswordChangeView):
    template_name = 'accounts/password.html'
    form_class = AccountPasswordForm
    success_url = reverse_lazy('accounts:profile')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        UserAction.record_action(request=self.request, action=UserAction.USER_ACTION.CHANGE_PASSWORD)
        if (valid := super().form_valid(form)): # noqa
            messages.success(self.request, 'Password Changed Successfully.')
        return valid


class AccountUpdateView(ProcessFormView):
    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileForm(instance=profile)
        return render(
            request=request,
            template_name='accounts/profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            instance=user,
            data=request.POST
        )
        profile_form = AccountProfileForm(
            instance=profile,
            data=request.POST,
            files=request.FILES,
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            pr = Profile.objects.get(user=user)
            old_image = pr.image.name
            profile_form.save()
            if old_image != profile_form.cleaned_data['image'].name:
                UserAction.record_image_action(request=request, old=old_image, new=request.FILES.get('image').name)
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='accounts/profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form
            }
        )

    def form_valid(self, form):
        UserAction.record_action(request=self.request, action=UserAction.USER_ACTION.CHANGE_PROFILE)
        if (valid := super().form_valid(form)): # noqa
            messages.success(self.request, 'Account Changed Successfully.')
        return valid
