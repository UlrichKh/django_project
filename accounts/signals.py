import os

from lms.settings import BASE_DIR, MEDIA_ROOT
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import Profile
from core.utils import find_duplicates


@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=Profile)
def post_save_profile(sender, instance, **kwargs):
    img = find_duplicates(instance.image.path, instance.PROFILE_IMAGES_DIRECTORY) # noqa
    if len(img) > 1:
        for item in img:
            if instance.image.name != item:
                os.remove(os.path.join(BASE_DIR, MEDIA_ROOT, item))
        instance.image = img[0]
        instance.save(update_fields=['image'])
