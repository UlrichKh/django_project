from django.urls import path

from . import views

app_name = "accounts"

urlpatterns = [
    path('register', views.AccountCreateView.as_view(), name='register'),
    path('login', views.AccountLoginView.as_view(), name='login'),
    path('logout', views.AccountLogoutView.as_view(), name='logout'),
    path('profile', views.AccountUpdateView.as_view(), name='profile'),
    path('password', views.AccountPasswordView.as_view(), name='password')
]
