import datetime as dt

from django.db import models
from django.contrib.auth.models import User

from .utils import generate_uuid


# Create your models here.
class BaseModel(models.Model):
    class Meta:
        abstract = True
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.write_date = dt.datetime.now()
        super().save(*args, **kwargs)


class Person(BaseModel):
    class Meta:
        abstract = True
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    birthdate = models.DateTimeField(null=True, default=dt.date.today)
    uuid = models.UUIDField(default=generate_uuid, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)

    def __str__(self):
        return f'{self.user.username} - {self.path}'
