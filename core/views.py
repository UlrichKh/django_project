from django.shortcuts import render, reverse, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class EditView:
    MODEL = None
    FORM = None
    REDIRECT_TO = ''
    PARAM_NAME = ''

    @classmethod
    def post(cls, request):
        form = cls.FORM(data=request.POST, instance=cls.item)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(f'{cls.REDIRECT_TO}:list'))

    @classmethod
    def edit(cls, request, uuid):
        cls.item = cls.MODEL.objects.get(uuid=uuid)
        if (_ := cls.post(request)) is not None: # noqa
            return _
        form = cls.FORM(instance=cls.item)
        return render(request, f'{cls.REDIRECT_TO}/edit.html', {
            "form": form,
            f"{cls.PARAM_NAME}": cls.item,
        })

    @classmethod
    def create(cls, request):
        form = cls.FORM()
        if (_ := cls.post(request)) is not None: # noqa
            return _
        return render(request, f'{cls.REDIRECT_TO}/create.html', {
            'form': form
        })

    @classmethod
    def delete(cls, request, uuid):
        get_object_or_404(cls.MODEL, uuid=uuid).delete()
        return HttpResponseRedirect(reverse(f"{cls.REDIRECT_TO}:list"))

    @classmethod
    def list(cls, request):
        list_object = cls.MODEL.objects.all()
        return render(request, f'{cls.REDIRECT_TO}/list.html', {
            f'{cls.REDIRECT_TO}': list_object
        })


class LoginRequiredBaseMixin(LoginRequiredMixin):
    login_url = 'accounts:index'


def index(request):
    return render(request, 'index.html')
