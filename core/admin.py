from django.contrib import admin  # noqa

from .models import Logger
# Register your models here.


admin.site.register(Logger)
