import copy

from core.models import Logger
import time
from urllib.parse import urlencode


class QueryParamsInjectorMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        query_params = copy.copy(request.GET)
        if 'page' in query_params:
            del query_params['page']
        request.query_params = urlencode(query_params)

        response = self.get_response(request)

        return response


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start = time.time()

        response = self.get_response(request)

        stop = time.time()
        user = request.user
        perf_time = stop-start
        query_params = request.query_params
        Logger.objects.create(
            user=user,
            path=request.path,
            execution_time=perf_time,
            query_params=query_params,
            info=f'{user.username} performed an action: method- {request.method},\
            status- {response.status_code} path-{request.path}'
        )

        return response
