import hashlib
import os
from uuid import uuid4


def generate_uuid():
    return uuid4().hex


def parse_count(request):
    count = request.GET.get('count')
    if count is None:
        raise EOFError("EOF ERROR: Couldn't find 'count' parameter.")
    if not count.isnumeric():
        raise ValueError("VALUE ERROR: 'Count' should be 'integer'")
    count = int(count)
    if not 0 < count < 100:
        raise ValueError(" RANGE ERROR: 'Count' parameter should be between 1 and 100.")
    return count


def find_duplicates(main_file, directory):
    list_of_duplicates = []
    hash_main_file = md5(main_file)
    for file in os.listdir(directory):
        file = os.path.join(directory, file)
        hash_file = md5(file)
        if hash_main_file == hash_file:
            list_of_duplicates.append(file)
    return list_of_duplicates


def md5(file_path):
    hash_file = hashlib.md5()
    with open(file_path, 'rb') as f:
        for chunk in iter(lambda: f.read(), b""):
            hash_file.update(chunk)
        return hash_file.hexdigest()
