from django.db import models


# Create your models here.
class Technology(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return f"{self.name}"


class COURSE_NAME(models.TextChoices):
    IT200 = "IT200", "Learn Python"
    IT201 = "IT201", "Python Basic Course"
    IT300 = "IT300", "Python Advanced and Ethical Hacking"
    IT400 = "IT400", "Django & Pyhton"
    IT410 = "IT410", "Django Complete Bootcamp"


class Course(models.Model):
    name = models.CharField(
        max_length=5,
        choices=COURSE_NAME.choices,
        default=COURSE_NAME.IT200
    )
    technologies = models.ManyToManyField(Technology, related_name='courses')

    def __str__(self):
        return f"{COURSE_NAME(self.name).label}"
