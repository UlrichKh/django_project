from django.contrib import admin

from .models import Course, Technology

# Register your models here.

admin.site.register(Course)
admin.site.register(Technology)
