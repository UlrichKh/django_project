import random
from faker import Faker
from .models import Student


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def gen_student():
    fake = Faker()
    first_name = fake.first_name()
    last_name = fake.last_name()
    birthday = fake.date_of_birth(tzinfo=None, minimum_age=20, maximum_age=99)
    st = Student(first_name=first_name, last_name=last_name, birthdate=birthday)
    st.save()
