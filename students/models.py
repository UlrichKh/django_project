import random

from faker import Faker
from django.db import models

from core.models import Person

# Create your models here.


class Student(Person):
    rating = models.SmallIntegerField(null=True, default=0)
    phone = models.CharField(null=True, max_length=13)

    @staticmethod
    def generate_students(count):
        faker = Faker()
        for _ in range(count):
            Student.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                rating=random.randint(0, 100),
            )
