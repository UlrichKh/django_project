from django.urls import path

from . import views

app_name = "students"

urlpatterns = [
    path('', views.hello, name='hello'),
    path('password', views.get_random, name="password"),
    path('generate_students', views.generate_students),
    path('list', views.StudentList.as_view(), name="list"),
    path('create', views.StudentCreateView.as_view(), name="create"),
    path('edit/<str:uuid>', views.StudentEditView.as_view(), name="edit"),
    path('delete/<str:uuid>', views.StudentDeleteView.as_view(), name="delete"),
]
