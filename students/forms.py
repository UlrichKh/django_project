from django import forms
from django.core.exceptions import ValidationError

from .models import Student


class StudentCreateForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = ("first_name", "last_name", "birthdate", "phone")

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone[0] == "+":
            phone = phone[1:]
        if not phone.isnumeric():
            raise ValidationError('Please.Input correct Phone number.')
        if not 8 < len(phone) < 12:
            raise ValidationError('Please. Check length of the Phone number.')
        return phone
