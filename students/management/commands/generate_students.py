from django.core.management.base import BaseCommand
from students.utils import gen_student


class Command(BaseCommand):
    help = "Generates provided number of Students into db"

    def add_arguments(self, parser):
        parser.add_argument(
            'number',
            type=int,
            help="Number of Students to generate"
            )

    def handle(self, *args, **options):
        for _ in range(options['number']):
            gen_student()
