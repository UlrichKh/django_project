from django.http import HttpResponse, HttpResponseBadRequest
from django.views.generic import UpdateView, ListView, CreateView, DeleteView
from django.urls import reverse_lazy

from core.views import LoginRequiredBaseMixin
from .models import Student
from .utils import gen_password, gen_student, parse_length
from .forms import StudentCreateForm
from core.utils import parse_count


# Create your views here.
class StudentList(ListView):
    model = Student
    context_object_name = 'students'
    template_name = 'students/list.html'
    paginate_by = 5

    def get_queryset(self):
        qs = super().get_queryset()
        request = self.request
        params = [
            'first_name',
            'last_name',
        ]
        for param in params:
            value = request.GET.get(param)
            if value:
                qs = qs.filter(**{param: value})
        return qs


class StudentEditView(LoginRequiredBaseMixin, UpdateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students/edit.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class StudentCreateView(LoginRequiredBaseMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students/create.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'
    pk_url_kwarg = 'uuid'


class StudentDeleteView(LoginRequiredBaseMixin, DeleteView):
    model = Student
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


def hello(request):
    return HttpResponse("Hello from Django!<br><a href='list'>List of students</a>")


def get_random(request):
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return HttpResponseBadRequest(str(ex))
    result = gen_password(length)
    return HttpResponse(result)


def generate_students(request):
    try:
        count = parse_count(request)
    except Exception as ex:
        return HttpResponseBadRequest(str(ex))
    for _ in range(count):
        gen_student()
    return HttpResponse(f"Generated {count} students")
