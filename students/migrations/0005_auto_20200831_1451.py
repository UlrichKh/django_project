# Generated by Django 3.1 on 2020-08-31 14:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0004_auto_20200831_1435'),
    ]

    operations = [
        migrations.RenameField(
            model_name='student',
            old_name='birthday',
            new_name='birthdate',
        ),
    ]
