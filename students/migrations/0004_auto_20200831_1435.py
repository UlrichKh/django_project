# Generated by Django 3.1 on 2020-08-31 14:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0003_auto_20200830_1559'),
    ]

    operations = [
        migrations.RenameField(
            model_name='student',
            old_name='birthdate',
            new_name='birthday',
        ),
    ]
